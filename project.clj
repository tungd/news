(defproject news "0.1.0-SNAPSHOT"
  :description "Naive Bayes News classifier"
  :url "http://bitbucket.org/tungd/news"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [hyperion/hyperion-sqlite "3.7.0"]
                 [http-kit "2.1.13"]
                 [com.syncthemall/boilerpipe "1.2.1"]]
  :jvm-opts ["-Dfile.encoding=UTF-8" "-server"])
