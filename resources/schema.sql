CREATE TABLE "source" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "url" text NOT NULL,
  "site" text NOT NULL,
  "category" text NOT NULL,
  "created_at" numeric NULL
);
CREATE UNIQUE INDEX "source_url" ON "source" ("url");

CREATE TABLE "article" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "url" text NOT NULL,
  "title" text NOT NULL,
  "content" text NOT NULL,
  "source_id" integer NOT NULL,
  "created_at" numeric NULL,
  FOREIGN KEY ("source_id") REFERENCES "source" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);
CREATE UNIQUE INDEX "article_url" ON "article" ("url");
