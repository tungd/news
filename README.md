# News

Naive Bayes News classifier written in Clojure.

## Problem
News aggregate services like Baomoi.com, Google News combines data from several sources
(news web sites) and re-categorise them into pre defined columns. This process is
performed automatically by computer.

> Note: Baomoi.com also recognizes articles that are duplicated between sources, even if
> they are slightly modified; However for our program this feature is optional and we
> might try to implement it if time allowed.

## Our approach

This project implement a Naive Bayes classifier to solve the stated problem.
The main knowledge defined in a Naive Bayes classifier are:

- Classes: In this case, our predefined categories for the articles.
- Features: We will be using simple bag-of-words model, so the features are the words
  contained in each articles and their frequency.

The detailed steps are:

- Collect the articles from news sources and store them in a SQlite database.
- Split the articles into words, then store them in a different table.
- Train the classifier with 35 40 45 50 55 60 65 70 75 percent of the collected data.

Database tables:

- source
  :id :url :site :category :created_at
- article
  :id :url :title :content :source_id :created_at

Feed Sources: See `resources/sources.csv`.

## Consideration for improvement

- Word segmentation
- Title priority
- Balanced training class

## Usage

    (load "news/core")

    # Import sources
    (load "news/source")
    (news.source/import "resources/sources/csv")

    # Collecting new article
    (news.core/do-collect)

    # Run the demo
    (news.core/demo)
    (news.core/demo 60) # with 60% of the samples used for training

    # Test/analysis
    (news.core/test)

## License

Copyright © 2013 Tung Dao <me@tungdao.com> and Long Pham <mr.j25th@gmail.com>

Distributed under the Eclipse Public License either version 1.0 or any later version.
