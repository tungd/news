(ns news.source
  (:refer-clojure :exclude [import])
  (require [clojure.string :as str]
           [hyperion.api :refer :all]
           [news.model :refer :all]))

(defn url->site [url]
  (first (first (re-seq #"((http|https):\/\/)?[^\/]+" url))))

(defn save-source [url cat]
  "Save the source along given category. Ensures that no duplication."
  (try (save (source :url url
                     :site (url->site url)
                     :category cat))
       (catch Exception e (println e))))

(defn import [csv]
  (let [lines (str/split-lines (slurp csv))]
    (doseq [[url cat] (map #(str/split % #",") lines)]
      (println url cat)
      (save-source url cat))))

(comment
  (delete-by-kind :source)

  (import "resources/sources.csv")
  (count-by-kind :source)
  (find-by-kind :source :limit 10)
  )
