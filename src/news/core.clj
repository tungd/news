(ns news.core
  (:gen-class)
  (:refer-clojure :exclude [test])
  (:require [hyperion.api :refer :all]
            [news.model :refer :all]
            [news.collect :as collect]
            [news.bayes :as bayes]
            [news.utils :refer [percent percentage]]))

(defn do-collect []
  "Collect new articles from sources."
  (collect/collect-all))


(defn samples
  "Returns some articles with random order."
  ([] (samples 250))
  ([size] (shuffle (find-by-kind :article :limit size))))

(defn do-train [articles ->features]
  (time (bayes/train-all
         (map (juxt article->category ->features) articles))))

(defn do-verify [articles data ->features]
  (let [verify-fn (fn [a]
                    (= (bayes/classify (->features a) data)
                       (article->category a)))]
    (time (count (filter verify-fn articles)))))


(defn do-test [sample-size train-size ->features]
  (let [articles (samples sample-size)
        trains (take train-size articles)
        tests (drop train-size articles)
        test-size (count tests)
        data (do-train articles ->features)
        correct (do-verify tests data ->features)
        accuracy (percentage correct test-size)]
    accuracy))

(defn test
  "Run test with default configuration, which is using 300 samples
with training size between 40% - 90%."
  []
  (let [size (count-by-kind :article), times 3, from 40, to 100]
    (doseq [s (range from to 10)]
      (let [test-fn (fn [_]
                      (do-test size (int (percent size s)) content->features))
            total (reduce + (map test-fn (range times)))]
        (println size "-" s "%: " (/ total times))))))

(defn do-demo [sample-size train-size ->features]
  "Anotated version of `do-test'."
  (let [articles (samples sample-size)
        trains (take train-size articles)
        tests (drop train-size articles)
        test-size (count tests)
        _ (println (format "Running with configuration: %d - %f%% (%d/%d)"
                           sample-size
                           (percentage train-size sample-size)
                           train-size sample-size))
        _ (println "Training...")
        data (do-train articles ->features)
        _ (println "All classes: " (keys data))
        _ (println "Verifying...")
        correct (do-verify tests data ->features)
        accuracy (percentage correct test-size)]
    (println
     (format "Done!. Accuracy: %f%% (%d/%d)" accuracy correct test-size))))

(defn demo
  "Run the demo training and classifying. Accept `size' as
with the percent of samples used for training is 80%."
  ([] (demo 80))
  ([size]
     (let [total (count-by-kind :article)]
       (do-demo total (int (percent total 80)) content->features))))

(defn -main [& args]
  "TODO: do something useful."
  )
