(ns news.model
  (:require [hyperion.api :refer :all]
            [hyperion.types :refer :all]
            [news.segment :as seg]))

(defn setup
  ([] (setup (new-datastore
              :implementation :sqlite
              :connection-url "jdbc:sqlite:resources/data.db")))
  ([ds] (set-ds! ds)))

(setup)

(defentity :article
  [id :type java.lang.Integer]
  [url :type java.lang.String]
  [title :type java.lang.String]
  [content :type java.lang.String]
  [source-key :type (foreign-key :source) :db-name :source-id]
  [created-at])

(defentity :source
  [id :type java.lang.Integer]
  [url :type java.lang.String]
  [site :type java.lang.String]
  [category :type java.lang.String]
  [created-at])

(defn article->source [a]
  (find-by-key (:source-key a)))

(def article->category
  (memoize (fn [a]
             (:category (article->source a)))))


(defn list-of-words [^String text]
  (distinct (seg/segment-max text)))

(defn title->features [a]
  (list-of-words (:title a)))


(defn bag-of-words [^String text]
  (frequencies (seg/segment-max text)))

(defn content->features [a]
  (bag-of-words (:content a)))
