(ns news.bayes)

(defn train
  "Train individual."
  ([cat feat] (train cat feat {}))
  ([cat feat data]
     (update-in data [cat] (partial merge-with + feat))))

(defn train-all
  "Train all the samples.
Returns the trained data. Accept optionally `feat-fn' that computes
the features from the samples.
"
  [samples]
  (reduce (fn [data [cat feat]]
            (train cat feat data)) {} samples))


(def total-feature
  (fn [feat data]
    (reduce + (map #(get % feat 0) (vals data)))))

(def p-feature
  (fn [feat cat data]
    (let [total (total-feature feat data)]
      (if (zero? total) 0.0
          (double (/ (get (data cat) feat 0) total))))))

(defn p-sample [feats cat data]
  (reduce * (for [[feat count] (seq feats)]
              (/ (+ 0.1 (* count (p-feature feat cat data)))
                 (+ count 1)))))

(defn classify [feats data]
  (apply max-key #(p-sample feats % data) (keys data)))
