(ns news.utils
  (:require [clojure.string :as str]
            [org.httpkit.client :as http]))

(def options
  "Fake a normal user request.
Dantri.com.vn has a captcha filter guard to counter DDos attack, these
headers help bypass it. Kid stuffs!"
  {:headers
   {"Accept"           "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Language"  "en-US,en;q=0.8,vi;q=0.6",
    "User-Agent"       "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.114 Safari/537.36",
    "Referer"          "http://google.com.vn/"
    }})

(defn fetch-url
  [url] (:body @(http/get url options)))


(defn percent [n p]
  (double (* n (/ p 100))))

(defn percentage [n m]
  (* 100 (double (/ n m))))
