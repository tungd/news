(ns news.segment
  "Text segmentation using various algorithm."
  (:require [clojure.string :as str]
            [clojure.set :as set]))


(def dict-path
  "Path to wordlist file. The wordlist is compiled by Ho Ngoc Duc.
`http://www.informatik.uni-leipzig.de/~duc/software/misc/wordlist.html'."
  "resources/vn_words/Viet39K.txt")

(def dict
  "Set of words from wordlist."
  (set (map str/lower-case (str/split-lines (slurp dict-path)))))

(defn syllable [^String word]
  "Returns the number of syllable for each words."
  (count (str/split word #"\p{Blank}")))

(defn unigrams [dict]
  (set/select #(= 1 (syllable %)) dict))

(defn bigrams [dict]
  (set/select #(= 2 (syllable %)) dict))

(defn ngrams [dict]
  (set/select #(not= 1 (syllable %)) dict))


(defn normalize
  "Remove non-meaningful charaters (digit, puntuation, symbols) from
the text. Also split the text into phrases based on punctuation."
  [^String text]
  (remove str/blank? (re-seq #"[\p{IsLatin}\p{Blank}]+" text)))

(defn tokenize
  "Splits the text based on whitespaces."
  [^String text]
  (remove str/blank? (str/split text #"\p{Blank}+")))


(defn segment-whitespace
  "Text segmentation based sole on whitespace."
  [^String text]
  (let [phrases (normalize text)]
    (apply concat (map tokenize phrases))))

(defn segment-max
  "Text segmentation using maximize-match (greedy) algorithm.
Use `segment-max-fn' under the hood."
  ([^String text]
     (let [phrases (normalize text)]
       (apply concat (map #(segment-max % true) phrases))))
  ([^String text single?]
     (loop [tokens (tokenize text)
            i (count tokens)
            segs []]
       (if (empty? tokens) segs
           (let [word (str/join " " (take i tokens))
                 rems (drop i tokens)]
             (if (or (= i 1) (contains? dict (str/lower-case word)))
               (recur rems (count rems) (conj segs word))
               (recur tokens (dec i) segs)))))))
