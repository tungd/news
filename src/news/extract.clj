(ns news.extract
  "Web content extractor using Boilerpipe (shallow text features).
More information: `https://code.google.com/p/boilerpipe/'."
  (:require [clojure.string :as str]
            [news.utils :refer [fetch-url]])
  (:import [de.l3s.boilerpipe.extractors ArticleExtractor]))

(defn normalize-whitespace
  [^String text]
  (str/replace text #"\u00A0" " "))

(defn url->content
  [^String url]
  (let [extractor (ArticleExtractor/getInstance)
        html (fetch-url url)]
    (normalize-whitespace (.getText extractor html))))
