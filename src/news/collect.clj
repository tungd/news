(ns news.collect
  (:require [clojure.xml :as xml]
            [hyperion.api :refer :all]
            [news.model :refer :all]
            [news.extract :refer [url->content]]))

(defn tag-content [node]
  (clojure.string/trim (first (:content node))))

(defn parse-item [node]
  (into {} (map (juxt :tag tag-content) (:content node))))

(defn parse-rss [url]
  (let [data (xml/parse url)
        items (filter #(= :item (:tag %)) (xml-seq data))]
    (map parse-item items)))

(defn item->url [data]
  (or (:link data) (:guid data)))

(defn item->article [data]
  (let [url (item->url data)
        content (url->content url)]
    (article :url url :title (:title data) :content content)))

(defn collect [source]
  "Collect new articles from the given source."
  (try (doseq [i (parse-rss (:url source))]
         (let [url (or (:link i) (:guid i))]
           (when (empty? (find-by-kind :article :filters [:= :url url]))
             (println "Processing: " url)
             (save (assoc (item->article i) :source-key (:key source))))))
       (catch Exception e (println e (:url source)))))

(defn collect-all
  "Collect new articles from all sources."
  []
  (doseq [src (find-by-kind :source)]
    (println "Getting articles from: " (:url src))
    (collect src)))
